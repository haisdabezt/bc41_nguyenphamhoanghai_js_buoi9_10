var mangThongBao = [
  "Vui lòng nhập tên tài khoản",
  "Vui lòng nhập họ tên",
  "Vui lòng nhập Email",
  "Vui lòng nhập mật khẩu",
  "Vui lòng chọn ngày",
  "Vui lòng nhập lương cơ bản",
  "Vui lòng chọn chức vụ",
  "Vui lòng nhập giờ làm",
  "Tên nhân viên phải là chữ",
  "Độ dài tài khoản 4 - 6 ký số",
  "Email không hợp lệ",
  "Mật khẩu từ 6-10 ký tự (ít nhất 1 số, 1 ký tự in hoa, 1 ký tự đặc biệt)",
  "Định dạng mm/dd/yyyy",
  "Lương cơ bản 1.000.000 - 20.000.000",
  "Số giờ làm trong tháng 80 - 200 giờ",
];

function getMyEle(ele) {
  return document.getElementById(ele);
}
function kiemTraNhap(idField, idThongBao, indexChuoiTB) {
  var kq = false;
  var valueField = getMyEle(idField).value;
  var thongBao = getMyEle(idThongBao);
  if (valueField === "") {
    thongBao.style.display = "block";
    thongBao.innerHTML = mangThongBao[indexChuoiTB];
    kq = false;
  } else {
    thongBao.style.display = "none";
    kq = true;
  }
  return kq;
}

function kiemTraDoDai(minLength, maxLength, indexChuoiTB) {
  var kq = false;
  var valueField = getMyEle("tknv").value;
  var thongBao = getMyEle("tbTKNV");
  var numbers = /^[0-9]+$/;

  if (
    valueField.match(numbers) &&
    valueField.length >= minLength &&
    valueField.length <= maxLength
  ) {
    thongBao.style.display = "none";
    kq = true;
  } else {
    thongBao.style.display = "block";
    thongBao.innerHTML = mangThongBao[indexChuoiTB];
    kq = false;
  }
  return kq;
}

function kiemTraNhapKiTu(idField, idThongBao, indexChuoiTB) {
  var kq = false;
  var valueField = getMyEle(idField).value;
  var thongBao = getMyEle(idThongBao);
  var letters = /^[A-Za-z]+$/;

  if (valueField.match(letters)) {
    thongBao.style.display = "none";
    kq = true;
  } else {
    thongBao.style.display = "block";
    thongBao.innerHTML = mangThongBao[indexChuoiTB];
    kq = false;
  }
  return kq;
}

function kiemTraEmail(idField, idThongBao, indexChuoiTB) {
  var kq = false;
  var valueField = getMyEle(idField).value;
  var thongBao = getMyEle(idThongBao);
  var mailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

  if (valueField.match(mailFormat)) {
    thongBao.style.display = "none";
    kq = true;
  } else {
    thongBao.style.display = "block";
    thongBao.innerHTML = mangThongBao[indexChuoiTB];
    kq = false;
  }
  return kq;
}

function kiemTraMatKhau() {
  var kq = false;
  var valueField = getMyEle("password").value;
  var thongBao = getMyEle("tbMatKhau");
  var checkMK =
    /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,10}$/;

  if (valueField.match(checkMK)) {
    thongBao.style.display = "none";
    kq = true;
  } else {
    thongBao.style.display = "block";
    thongBao.innerHTML = mangThongBao[11];
    kq = false;
  }
  return kq;
}

function kiemTraNgay() {
  var kq = false;
  var theSelect = getMyEle("datepicker").value;
  var thongBao = getMyEle("tbNgay");
  var dateFormat =
    /^(0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])[\/\-]\d{4}$/;

  if (theSelect.match(dateFormat)) {
    thongBao.innerHTML = "none";
    kq = true;
  } else {
    thongBao.style.display = "block";
    thongBao.innerHTML = mangThongBao[12];
    kq = false;
  }
  return kq;
}

function luongCoBan(minLuong, maxLuong, indexChuoiTB) {
  var kq = false;
  var valueField = getMyEle("luongCB").value;
  var thongBao = getMyEle("tbLuongCB");

  if (valueField >= minLuong && valueField <= maxLuong) {
    thongBao.innerHTML = "none";
    kq = true;
  } else {
    thongBao.style.display = "block";
    thongBao.innerHTML = mangThongBao[indexChuoiTB];
    kq = false;
  }
  return kq;
}

function kiemTraChucVu(indexChuoiTB) {
  var kq = false;
  var theSelect = getMyEle("chucvu");
  var thongBao = getMyEle("tbChucVu");

  if (theSelect.selectedIndex == "") {
    thongBao.style.display = "block";
    thongBao.innerHTML = mangThongBao[indexChuoiTB];
    kq = false;
  } else {
    thongBao.style.display = "none";
    kq = true;
  }
  return kq;
}

function soGioLamViec(minGio, maxGio, indexChuoiTB) {
  var kq = false;
  var valueField = getMyEle("gioLam").value;
  var thongBao = getMyEle("tbGiolam");

  if (valueField >= minGio && valueField <= maxGio) {
    thongBao.innerHTML = "none";
    kq = true;
  } else {
    thongBao.style.display = "block";
    thongBao.innerHTML = mangThongBao[indexChuoiTB];
    kq = false;
  }
  return kq;
}

function kiemTraHopLe() {
  var kq = kiemTraNhap("tknv", "tbTKNV", 0);

  if (kq) {
    kq = kiemTraDoDai(4, 6, 9);
  }

  kq = kiemTraNhap("ten", "tbTen", 1);
  if (kq) {
    kq = kiemTraNhapKiTu("ten", "tbTen", 8);
  }

  kq = kiemTraNhap("email", "tbEmail", 2);
  if (kq) {
    kq = kiemTraEmail("email", "tbEmail", 10);
  }

  kq = kiemTraNhap("password", "tbMatKhau", 3);
  if (kq) {
    kq = kiemTraMatKhau();
  }
  kq = kiemTraNhap("datepicker", "tbNgay", 4);
  if (kq) {
    kq = kiemTraNgay();
  }

  kq = kiemTraNhap("luongCB", "tbLuongCB", 5);
  if (kq) {
    kq = luongCoBan(1000000, 20000000, 13);
  }

  kq = kiemTraChucVu(6);

  kq = kiemTraNhap("gioLam", "tbGiolam", 7);
  if (kq) {
    kq = soGioLamViec(80, 200, 14);
  }
  return kq;
}

var dsNhanVien = new DanhSachNV();
function taoDongTable(tBody, nv) {
  var tr = document.createElement("tr");
  tBody.appendChild(tr);
  for (var i = 0; i < nv.mangNhanVien.length; i++) {
    var td = document.createElement("td");
    td.innerHTML = nv.mangNhanVien[i];
    tr.appendChild(td);
  }

  var btnSua =
    "<button type='button' class='btn btn-primary' value='Sua' id='sua_" +
    nv.taiKhoan +
    "'>Sua</button>";
  var btnCapNhat =
    "<button type='button' class='btn btn-primary btnCapNhat' value='capnhat' id='capnhat_" +
    nv.taiKhoan +
    "'>Cập nhật</button>";
  var btnXoa =
    "<button type='button' class='btn btn-primary' value='xoa' id='xoa_" +
    nv.taiKhoan +
    "'>Xóa</button>";

  var td = document.createElement("td");
  td.innerHTML = btnSua + btnCapNhat + btnXoa;
  tr.appendChild(td);
}

function xuLyThemNhanvienVaoTable() {
  var tBody = getMyEle("tableDanhSach");
  var soNhanVien = dsNhanVien.soLuongNhanVien();
  dsNhanVien.xuatThuocTinhNV();
  tBody.innerHTML = "";
  for (var i = 0; i < soNhanVien; i++) {
    var nhanVien = dsNhanVien.mangNhanVien[i];
    taoDong(tBody, nhanVien);
  }
}

function deleteHandler(ele) {
  getMyEle(ele).addEventListener("click", function () {
    var table = getMyEle("tableHienThi");
    alert(this.id);
  });
}

function editHandler(ele) {
  getMyEle(ele).addEventListener("click", function () {
    var table = getMyEle("tableHienThi");
    alert(this.id);
  });
}

function lamMoiForm() {
  getMyEle("tknv").value = "";
  getMyEle("ten").value = "";
  getMyEle("email").value = "";
  getMyEle("password").value = "";
  getMyEle("datepicker").value = "";
  getMyEle("luongCB").value = "";
  getMyEle("chucvu").selectedIndex = 0;
  getMyEle("gioLam").value = "";
}

getMyEle("btnThemNV").addEventListener("click", function () {
  var ktraHopLe = kiemTraHopLe();
  if (ktraHopLe) {
    var taiKhoan = getMyEle("tknv").value;
    var ten = getMyEle("ten").value;
    var email = getMyEle("email").value;
    var password = getMyEle("password").value;
    var ngayLV = getMyEle("datepicker").value;
    var luongCB = getMyEle("luongCB").value;
    var chucVu = getMyEle("chucvu").value;
    var gioLam = getMyEle("gioLam").value;

    var nhanVien = new NhanVien(
      taiKhoan,
      ten,
      email,
      password,
      ngayLV,
      luongCB,
      chucVu,
      gioLam
    );
    dsNhanVien.themNhanVien(nhanVien);
    lamMoiForm();

    var hienThi = getMyEle("hienthi");
    xuLyThemNhanvienVaoTable();
  }
});
