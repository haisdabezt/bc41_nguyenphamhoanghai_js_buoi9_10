function NhanVien(
  taiKhoan,
  ten,
  email,
  password,
  ngayLV,
  luongCB,
  chucVu,
  gioLam
) {
  this.taiKhoan = taiKhoan;
  this.ten = ten;
  this.email = email;
  this.password = password;
  this.ngayLV = ngayLV;
  this.luongCB = luongCB;
  this.chucVu = chucVu;
  this.gioLam = gioLam;

  this.mangNhanVien = [
    this.taiKhoan,
    this.ten,
    this.email,
    this.password,
    this.ngayLV,
    this.luongCB,
    this.chucVu,
    this.gioLam,
  ];
}

NhanVien.prototype.tinhLuongGD = function () {
  return this.luongCoBan * 3;
};
